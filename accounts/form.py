from django import forms
from django.forms.widgets import TextInput, Textarea, Select, FileInput
from django.utils.translation import ugettext_lazy as _
from accounts.models import SurveyDetails


class SurveyDetailsForm(forms.ModelForm):

    class Meta:
        model = SurveyDetails
        fields = '__all__'
 
        widgets = {
            'land_document' : FileInput(attrs={'class' : 'required form-control'}),
            'survey_number' : TextInput(attrs={"class":"required form-control","placeholder":"Survey Number"}),
            'block_number' : TextInput(attrs={"class":"required form-control","placeholder":"Block Number"}),
            'district' : Select(attrs={'class': 'required form-control'}),
            'taluk' : TextInput(attrs={"class":"required form-control","placeholder":"Taluk"}),
            'village' : TextInput(attrs={"class":"required form-control","placeholder":"Village"}),

        }
        error_messages = {
        	'land_document' : {
                'required' : _("Land Document field is required."),
            },
            'survey_number' : {
                'required' : _("Email Type field is required."),
            },
     		'block_number' : {
                'required' : _("Phone Type field is required."),
            },
            'district' : {
                'required' : _("Message Type field is required."),
            },
     		'taluk' : {
                'required' : _("Phone Type field is required."),
            },
            'village' : {
                'required' : _("Message Type field is required."),
            }

        }
