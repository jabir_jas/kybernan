from django.urls import include, path
from rest_framework.routers import DefaultRouter

from accounts.views import UserProfileListCreateView, userProfileDetailView, UserTokenObtainPairView

from rest_framework_simplejwt.views import TokenRefreshView

urlpatterns = [
    #gets all user profiles and create a new profile
    path("accounts/all-profiles",UserProfileListCreateView.as_view(),name="all-profiles"),
   # retrieves profile details of the currently logged in user
    path("accounts/profile/<int:pk>",userProfileDetailView.as_view(),name="profile"),

    path('token/', UserTokenObtainPairView.as_view(), name='user_token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]