from django.conf.urls import url,include
from django.urls import path
from django.contrib import admin

from accounts import views as accounts_views

urlpatterns = [
    path('api/blogs/', include('blogs.urls')),
    path('api/shopping-cart/', include('cart_item.urls')),
    path('api/',include("accounts.urls")),
    url(r'^admin/', admin.site.urls),

    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.jwt')),

]