from django.urls import path
from django.conf.urls import url, include
from blogs import views

urlpatterns = [
    path('', views.blog_item,name='blog_item'),
    url(r'^(?P<pk>[\w-]+)/$',views.blog_list,name='blog_list'),
]

