from django.db import models
from django.contrib.auth.models import User
import datetime
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import slugify
from random import randint
from django.contrib.postgres.fields import ArrayField


class Blog(models.Model):
    title = models.CharField(max_length=300)
    author= models.CharField(max_length=128)
    date = models.DateField(_("Date"), default=datetime.date.today)
    description = models.TextField()
    slug = models.SlugField(primary_key=True,unique = True, max_length=128,blank=True)
    image = models.ImageField(upload_to ='uploads/')
    category = ArrayField(models.CharField(max_length=200))
    tags = ArrayField(models.CharField(max_length=200))


    active = models.BooleanField(default=True, db_index=True)
    added_by = models.ForeignKey(User, null=True, blank=True, related_name="Blog_added_by", on_delete=models.SET_NULL)
    edited_by = models.ForeignKey(User, null=True, blank=True, related_name="Blog_edited_by", on_delete=models.SET_NULL)
    created_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_time = models.DateTimeField(auto_now=True, null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.title)
            exists = Blog.objects.filter(slug=self.slug).exists()
            while exists:
                self.slug += "-" + str(randint(1000,9999))
                exists = Blog.objects.filter(slug=self.slug).exists()

        super(Blog, self).save(*args, **kwargs)

    class Meta:
        db_table = 'blog_item'
        verbose_name = _('Blog Content')
        verbose_name_plural = _('Blog Contents')
        ordering = ['-created_time']

    def __str__(self):
        return self.title
