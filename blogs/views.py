from django.shortcuts import render
from rest_framework import serializers
from blogs.models import Blog
from django.db.models import Q

from rest_framework.response import Response
from rest_framework import status, exceptions
from django.http import HttpResponse

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated


class BlogSerializer(serializers.ModelSerializer):
	class Meta:
		model = Blog
		fields = ['title', 'author', 'date', 'description', 'slug', 'image', 'category', 'tags', 'active']


@api_view(['GET','POST'])
def blog_item(request):

	#list all blog or create a new one

	if request.method =='GET':
		blog = Blog.objects.all()
		serializer = BlogSerializer(blog, many=True)
		data = serializer.data

		if 'search' in request.GET and request.GET['search']:
			search = request.GET['search']
			blog = blog.filter(Q(title__icontains=search)|Q(description__icontains=search)|Q(author__icontains=search)|Q(category__icontains=search)|Q(tags__icontains=search))
			serializer = BlogSerializer(blog, many=True)
			data = serializer.data

		page = 0
		limit = 10
		if 'page' in request.GET and request.GET['page']:
			try:
				page = int(request.GET['page'])
			except:
				pass
	 
		return Response(data[page*limit:page*limit + limit])

	elif request.method =='POST':
		if request.user.is_superuser:
			serializer = BlogSerializer(data=request.data)
			if serializer.is_valid():
				serializer.save()
				return Response(serializer.data, status=status.HTTP_201_CREATED)
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		else:
			return Response({'error': 'Invalid credentials given'}, status=status.HTTP_401_UNAUTHORIZED)




@api_view(['GET', 'PUT', 'DELETE'])
def blog_list(request, pk):
	"""
	Retrieve, update or delete a code blog.
	"""
	try:
		blog = Blog.objects.get(pk=pk)
	except Blog.DoesNotExist:
		return HttpResponse(status=status.HTTP_404_NOT_FOUND)

	if request.method == 'GET':
		serializer = BlogSerializer(blog)
		return Response(serializer.data)

	elif request.method == 'PUT':
		if request.user.is_superuser:
			serializer = BlogSerializer(blog, data=request.data)
			if serializer.is_valid():
				serializer.save()
				return Response(serializer.data)
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
		else:
			return Response({'error': 'Invalid credentials given'}, status=status.HTTP_401_UNAUTHORIZED)

	elif request.method == 'DELETE':
		if request.user.is_superuser:
			blog.delete()
			return Response(status=status.HTTP_204_NO_CONTENT)
		else:
			return Response({'error': 'Invalid credentials given'}, status=status.HTTP_401_UNAUTHORIZED)
