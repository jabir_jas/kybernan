from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class Subscribe(models.Model):
    email = models.EmailField()

    active = models.BooleanField(default=True, db_index=True)
    added_by = models.ForeignKey(User, null=True, blank=True, related_name="subscribe_added_by", on_delete=models.SET_NULL)
    edited_by = models.ForeignKey(User, null=True, blank=True, related_name="subscribe_edited_by", on_delete=models.SET_NULL)
    created_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_time = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'subscribes'
        verbose_name = _('subscribe')
        verbose_name_plural = _('subscribes')

    def __str__(self):
        return self.email