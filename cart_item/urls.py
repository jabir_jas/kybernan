from django.conf.urls import url, include
from rest_framework import routers
from cart_item import views

router = routers.DefaultRouter()
router.register(r'audit', views.DetailedAccountViewSet, basename='DetailedAccount')
router.register(r'account', views.AccountViewSet, basename='Account')
router.register(r'cart', views.CartViewSet, basename='Cart')
router.register(r'addresses', views.AddressViewSet, basename='Address')
router.register(r'products', views.ProductViewSet, basename='Product')
router.register(r'orders', views.OrderViewSet, basename='Order')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browseable API.
urlpatterns = [
    url(r'^', include(router.urls)),
]

