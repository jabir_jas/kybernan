from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
import uuid

class Address(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name="addresses")
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    street_address1 = models.CharField(max_length=255)
    street_address2 = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    zip_code = models.CharField(max_length=255)
    phone = models.CharField(max_length=255, blank=True, null=True)

    active = models.BooleanField(default=True, db_index=True)
    added_by = models.ForeignKey(User, null=True, blank=True, related_name="address_added_by", on_delete=models.SET_NULL)
    edited_by = models.ForeignKey(User, null=True, blank=True, related_name="address_edited_by", on_delete=models.SET_NULL)
    created_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_time = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        db_table = 'cart_addresses'
        verbose_name = _('Address Content')
        verbose_name_plural = _('Address Contents')
        ordering = ['-created_time']

    def __str__(self):
        return self.id


class Product(models.Model):
    status = models.CharField(max_length=255)
    product_name = models.CharField(max_length=255)
    original_price = models.DecimalField(max_digits=10, decimal_places=2)
    our_price = models.DecimalField(max_digits=10, decimal_places=2)
    origianl_url = models.TextField()
    image_url = models.TextField()
    html = models.TextField()
    options = models.TextField()
    imageData = models.TextField()

    active = models.BooleanField(default=True, db_index=True)
    added_by = models.ForeignKey(User, null=True, blank=True, related_name="product_added_by", on_delete=models.SET_NULL)
    edited_by = models.ForeignKey(User, null=True, blank=True, related_name="product_edited_by", on_delete=models.SET_NULL)
    created_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_time = models.DateTimeField(auto_now=True, null=True, blank=True)
    uuid = models.CharField(max_length=50, default=uuid.uuid4, db_index=True, primary_key=True)


    class Meta:
        db_table = 'cart_products'
        verbose_name = _('Product Content')
        verbose_name_plural = _('Product Contents')
        ordering = ['-created_time']

    def __str__(self):
        return self.id

 
class Cart(models.Model):
    status = models.CharField(max_length=255, blank=True, null=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name="carts")
    items = models.ManyToManyField(Product) 

    active = models.BooleanField(default=True, db_index=True)
    added_by = models.ForeignKey(User, null=True, blank=True, related_name="cart_added_by", on_delete=models.SET_NULL)
    edited_by = models.ForeignKey(User, null=True, blank=True, related_name="cart_edited_by", on_delete=models.SET_NULL)
    created_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_time = models.DateTimeField(auto_now=True, null=True, blank=True)


    class Meta:
        db_table = 'cart'
        verbose_name = _('Cart Content')
        verbose_name_plural = _('Cart Contents')
        ordering = ['-created_time']

    def __str__(self):
        return self.id   


class Order(models.Model):
    cart = models.ForeignKey(Cart,on_delete=models.SET_NULL, blank=True, null=True)
    items = models.ManyToManyField(Product) 
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name="orders")

    active = models.BooleanField(default=True, db_index=True)
    added_by = models.ForeignKey(User, null=True, blank=True, related_name="order_added_by", on_delete=models.SET_NULL)
    edited_by = models.ForeignKey(User, null=True, blank=True, related_name="order_edited_by", on_delete=models.SET_NULL)
    created_time = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_time = models.DateTimeField(auto_now=True, null=True, blank=True)


    class Meta:
        db_table = 'cart_orders'
        verbose_name = _('Order Content')
        verbose_name_plural = _('Order Contents')
        ordering = ['-created_time']

    def __str__(self):
        return self.id
