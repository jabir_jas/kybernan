from django.apps import AppConfig


class CartItemConfig(AppConfig):
    name = 'cart_item'
